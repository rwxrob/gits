package gits

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func NewAgent() *agent {
	agent := new(agent)
	agent.LoadConfig()
	return agent
}

type agent struct {
	gl *gitlab
}

func (g *agent) LoadConfig(args ...string) (err error) {
	var path string

	switch len(args) {
	case 0:
		config, err := os.UserConfigDir()
		if err != nil {
			return err
		}
		path = filepath.Join(config, "gits", "config.json")
	case 1:
		path = args[0]
	default:
		return fmt.Errorf("loadconfig: takes one directory path argument ")
	}

	var byt []byte
	byt, err = ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	// safer than built-in unmarshalling
	c := map[string]interface{}{}
	err = json.Unmarshal(byt, &c)
	if err != nil { // only checks for JSON syntax errors
		return err
	}

	var v interface{}
	var have bool

	// gitlab
	v, have = c["gitlab"]
	if !have {
		return fmt.Errorf("config: missing GitLab data")
	}
	g.gl = new(gitlab)

	// token
	tok, have := v.(map[string]interface{})["token"]
	if !have {
		return fmt.Errorf("config: missing GitLab personal access token")
	}
	g.gl.tok = tok.(string)

	// name
	name, have := v.(map[string]interface{})["name"]
	if !have {
		return fmt.Errorf("config: missing GitLab name")
	}
	g.gl.name = name.(string)

	// user
	user, have := v.(map[string]interface{})["user"]
	if !have {
		return fmt.Errorf("config: missing GitLab user")
	}
	g.gl.user = user.(string)

	// email
	email, have := v.(map[string]interface{})["email"]
	if !have {
		return fmt.Errorf("config: missing gitlab email")
	}
	g.gl.email = email.(string)

	return nil
}

func (a *agent) Ping(what string) error {
	switch {
	case strings.HasPrefix(what, "gitlab.com"):
		return a.gl.ping()
	}
	return fmt.Errorf("ping: failed for %v", what)
}

func (a *agent) Create(what string) error {
	switch {
	case strings.HasPrefix(what, "gitlab.com"):
		name := what[11:]
		return a.gl.create(name)
	}
	return fmt.Errorf("create: failed for %v", what)
}

func Create(what string) error {
	return NewAgent().Create(what)
}

/*
func (a *agent) Userid(what string) (string, error) {
	switch {
	case strings.HasPrefix("gitlab.com", what):
		return a.gl.userid()
	}
	return "", fmt.Errorf("userid: failed for %v", what)
}
*/
