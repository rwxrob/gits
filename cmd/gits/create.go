package main

import (
	"gitlab.com/rwxrob/cmdtab"
	"gitlab.com/rwxrob/gits"
)

func init() {
	_cmd := cmdtab.New("create")
	_cmd.Summary = `Create a new git project repo`
	_cmd.Description = ``
	_cmd.Method = func(args []string) error {
		switch len(args) {
		case 1:
			return gits.Create(args[0])
		default:
			return _cmd.UsageError()
		}
		return nil
	}
}
