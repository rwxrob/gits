package gits

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type gitlab struct {
	name  string
	user  string
	email string
	tok   string
}

func (g *gitlab) send(byt []byte, url string) (out string, err error) {
	req, err := http.NewRequest("POST", "https://gitlab.com/"+url, bytes.NewBuffer(byt))
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Bearer "+g.tok)
	req.Header.Add("Content-Type", "application/json")
	client := new(http.Client)
	client.Timeout = time.Second * 10
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (g *gitlab) graphql(in interface{}) (out string, err error) {
	var byt []byte
	switch v := in.(type) {
	case string:
		byt, err = json.Marshal(map[string]string{"query": v})
	default:
		return "", fmt.Errorf("graphql: invalid input type")
	}
	if err != nil {
		return "", err
	}
	return g.send(byt, "/api/graphql")
}

func (g *gitlab) ping() error {
	res, err := g.graphql(`query {currentUser {name}}`)
	if err != nil {
		return err
	}
	good := fmt.Sprintf(`{"data":{"currentUser":{"name":"%v"}}}`, g.name)
	if res != good {
		return fmt.Errorf("ping: failed to authenticate: %v\n", res)
	}
	return nil
}

/*
func (g *gitlab) userid() (int, error) {
	query := fmt.Sprintf(`
{
  user(username:"%v") {
    id
  }
}`, g.user)
	res, err := g.graphql(query)
	if err != nil {
		return "", err
	}
	id := 0
	return id, nil
}
*/

func (g *gitlab) create(in interface{}) error {
	var byt []byte
	var err error
	switch v := in.(type) {
	case string:
		byt, err = json.Marshal(map[string]string{"name": v})
	}
	_, err = g.send(byt, "/api/v4/projects")
	return err
}
