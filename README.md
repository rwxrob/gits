# Gits: Universal Git Service Library and Command Utility

Tired of having a different CRUD tool for every Git hosting service?
Maybe `gits` is for you.

## Configuration

In order to use Gits you first need to create personal access tokens for
the services you intend to use in the configuration file `~/.config/gits/config.json`.

## Testing

Testing depends on having accounts on the Git hosting services required
with personal access tokens saved in the configuration files. It is
recommended that you create a sample/dummy account on each service for
such work (which is usually permitted under the terms and services of
most hosting providers). See the
[testdata/config.json](testdata/config.json) for an example. You will
need to pass the path to the directory containing a `config.json` file
in order for the tests to work. This is to prevent the saving of
personal access tokens within the test cases.

```
got test -confdif /tmp/mytmpgitsconf
```
