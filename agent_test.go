package gits

import (
	"flag"
	"fmt"
	"testing"
)

var confdir string

func init() {
	flag.StringVar(&confdir, "confdir", "", "Gits Configuration Directory")
}

func TestAgent_Ping(t *testing.T) {
	if confdir == "" {
		t.Errorf("-confdir not set")
	}
	agent := new(agent)
	if e := agent.LoadConfig(confdir); e != nil {
		t.Fatal(e)
	}
	if e := agent.Ping("gitlab.com"); e != nil {
		t.Fatal(e)
	}
}

func TestAgent_Create(t *testing.T) {
	if confdir == "" {
		t.Errorf("-confdir not set")
	}
	agent := new(agent)
	if e := agent.LoadConfig(confdir); e != nil {
		t.Fatal(e)
	}
	if e := agent.Create("gitlab.com/boo"); e != nil {
		t.Fatal(e)
	}
}

/*
func TestAgent_Userid(t *testing.T) {
	if confdir == "" {
		t.Errorf("-confdir not set")
	}
	agent := new(agent)
	if e := agent.LoadConfig(confdir); e != nil {
		t.Fatal(e)
	}
	id, e := agent.Userid("gitlab.com")
	if e != nil {
		t.Fatal(e)
	}
	fmt.Println(id)
}
*/

func TestAgent_LoadConfig(t *testing.T) {
	agent := new(agent)
	err := agent.LoadConfig("testdata")
	if err != nil {
		t.Error(err)
	}
	if fmt.Sprintf("%v", agent.gl) != `&{RWX You rwxyou rwxyou@pm.me some}` {
		t.Error("failed to load gitlab config")
	}
}
